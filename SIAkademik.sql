-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             10.1.0.5505
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for SIAkademik
CREATE DATABASE IF NOT EXISTS `SIAkademik` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `SIAkademik`;

-- Dumping structure for table SIAkademik.ACTIVITY
CREATE TABLE IF NOT EXISTS `ACTIVITY` (
  `ACTIVITY_TYPE` varchar(50) DEFAULT NULL,
  `ACITIVITY_NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.ACTIVITY: ~0 rows (approximately)
/*!40000 ALTER TABLE `ACTIVITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACTIVITY` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.GURU
CREATE TABLE IF NOT EXISTS `GURU` (
  `ID_GURU` varchar(255) NOT NULL,
  `AGAMA` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `JABATAN` varchar(255) DEFAULT NULL,
  `LULUSAN` varchar(255) DEFAULT NULL,
  `NAMA` varchar(255) DEFAULT NULL,
  `NIP` varchar(255) DEFAULT NULL,
  `STATUS_GURU` varchar(255) DEFAULT NULL,
  `TANGGAL_LAHIR` varchar(255) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(255) DEFAULT NULL,
  `TELEPHONE` varchar(255) DEFAULT NULL,
  `guru_ID_GURU` varchar(255) DEFAULT NULL,
  `ID_GURU2` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_GURU2`),
  UNIQUE KEY `UK_pcjebkqrfe44f30mrbvvcksqy` (`ID_GURU`),
  UNIQUE KEY `UK_kpge8nxd3iew7ibmrkb78xlh7` (`EMAIL`),
  UNIQUE KEY `UK_e5mw7406we6mf9mard1ipykxm` (`NIP`),
  UNIQUE KEY `UK_hy0b382jvwiwlx6n7fu0sggwi` (`TELEPHONE`),
  KEY `FKm7143jlh7hpbst3hpe5e1xfdr` (`guru_ID_GURU`),
  CONSTRAINT `FKiiejcovy0swhkiaqwhxtffvsj` FOREIGN KEY (`ID_GURU2`) REFERENCES `JADWAL_PELAJARAN` (`ID_JADWAL`),
  CONSTRAINT `FKm7143jlh7hpbst3hpe5e1xfdr` FOREIGN KEY (`guru_ID_GURU`) REFERENCES `GURU` (`ID_GURU2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.GURU: ~0 rows (approximately)
/*!40000 ALTER TABLE `GURU` DISABLE KEYS */;
/*!40000 ALTER TABLE `GURU` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.JADWAL_PELAJARAN
CREATE TABLE IF NOT EXISTS `JADWAL_PELAJARAN` (
  `ID_JADWAL` varchar(255) NOT NULL,
  `DARI` varchar(255) NOT NULL,
  `HARI` varchar(255) NOT NULL,
  `ID_GURU` varchar(255) NOT NULL,
  `ID_KELAS` varchar(255) NOT NULL,
  `ID_PELAJARAN` varchar(255) NOT NULL,
  `ID_TAHUN_AJARAN` varchar(255) NOT NULL,
  `JAM` varchar(255) NOT NULL,
  `SAMPAI` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_JADWAL`),
  UNIQUE KEY `UK_iuw1y83wpx7kkk1tw7ep2hpws` (`HARI`),
  UNIQUE KEY `UK_nlqa1eibd18fp20x7x7y0cp2p` (`ID_KELAS`),
  UNIQUE KEY `UK_gf9j247ux74akb17yr3uatlb2` (`ID_TAHUN_AJARAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.JADWAL_PELAJARAN: ~0 rows (approximately)
/*!40000 ALTER TABLE `JADWAL_PELAJARAN` DISABLE KEYS */;
/*!40000 ALTER TABLE `JADWAL_PELAJARAN` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.KELAS
CREATE TABLE IF NOT EXISTS `KELAS` (
  `ID_KELAS` varchar(255) NOT NULL,
  `KELAS` varchar(255) DEFAULT NULL,
  `ID_TAHUN_AJARAN` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_KELAS`),
  KEY `FKs05d99v1d7tl4vgobtpjwj23v` (`ID_TAHUN_AJARAN`),
  CONSTRAINT `FKs05d99v1d7tl4vgobtpjwj23v` FOREIGN KEY (`ID_TAHUN_AJARAN`) REFERENCES `TAHUN_AJARAN` (`ID_TAHUN_AJARAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.KELAS: ~0 rows (approximately)
/*!40000 ALTER TABLE `KELAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `KELAS` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.MATA_PELAJARAN
CREATE TABLE IF NOT EXISTS `MATA_PELAJARAN` (
  `ID_PELAJARAN` varchar(255) NOT NULL,
  `PELAJARAN` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_PELAJARAN`),
  UNIQUE KEY `UK_sb8yplrfhhvnc01l8nwe2yd35` (`PELAJARAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.MATA_PELAJARAN: ~0 rows (approximately)
/*!40000 ALTER TABLE `MATA_PELAJARAN` DISABLE KEYS */;
/*!40000 ALTER TABLE `MATA_PELAJARAN` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.ROLE
CREATE TABLE IF NOT EXISTS `ROLE` (
  `ROLE_ID` int(11) DEFAULT NULL,
  `ROLE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.ROLE: ~4 rows (approximately)
/*!40000 ALTER TABLE `ROLE` DISABLE KEYS */;
INSERT INTO `ROLE` (`ROLE_ID`, `ROLE`) VALUES
	(0, 'SUPER_ADMIN'),
	(1, 'ADMIN'),
	(2, 'GURU'),
	(3, 'SISWA');
/*!40000 ALTER TABLE `ROLE` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.TAHUN_AJARAN
CREATE TABLE IF NOT EXISTS `TAHUN_AJARAN` (
  `ID_TAHUN_AJARAN` varchar(255) NOT NULL,
  `TAHUN_AJARAN` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_TAHUN_AJARAN`),
  UNIQUE KEY `UK_dv0uga39pfifjiyjr8jmtk9m1` (`TAHUN_AJARAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.TAHUN_AJARAN: ~0 rows (approximately)
/*!40000 ALTER TABLE `TAHUN_AJARAN` DISABLE KEYS */;
/*!40000 ALTER TABLE `TAHUN_AJARAN` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USERS
CREATE TABLE IF NOT EXISTS `USERS` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `FULL_NAME` text,
  `ACTIVE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `Index 1` (`USER_ID`,`EMAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USERS: ~4 rows (approximately)
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
INSERT INTO `USERS` (`USER_ID`, `USERNAME`, `PASSWORD`, `EMAIL`, `FULL_NAME`, `ACTIVE`) VALUES
	(1, 'super', 'super', 'super@gmail.com', 'SUPER ADMIN', '1'),
	(2, 'admin', 'admin', 'admin@gmail.com', 'ADMIN', '1'),
	(3, 'guru', 'guru', 'guru@gmail.com', 'GURU', '1'),
	(4, 'siswa', 'siswa', 'siswa@gmail.com', 'SISWA', '1');
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USER_ACTIVITY
CREATE TABLE IF NOT EXISTS `USER_ACTIVITY` (
  `ID` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `ACTIVITY_TYPE` varchar(50) DEFAULT NULL,
  `DATE_ACTIVITY` datetime DEFAULT NULL,
  `MODULE_NAME` varchar(100) DEFAULT NULL,
  `MODULE_CODE` varchar(50) DEFAULT NULL,
  `IP_ADDRESS` varchar(100) DEFAULT NULL,
  `USER_AGENT` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USER_ACTIVITY: ~6 rows (approximately)
/*!40000 ALTER TABLE `USER_ACTIVITY` DISABLE KEYS */;
INSERT INTO `USER_ACTIVITY` (`ID`, `USER_ID`, `ACTIVITY_TYPE`, `DATE_ACTIVITY`, `MODULE_NAME`, `MODULE_CODE`, `IP_ADDRESS`, `USER_AGENT`) VALUES
	(00000000001, 1, NULL, '2019-03-05 17:22:19', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000002, 1, NULL, '2019-03-05 17:23:24', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000003, 1, NULL, '2019-03-05 17:24:09', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000004, 1, NULL, '2019-03-05 17:24:44', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000005, 1, NULL, '2019-03-05 17:24:49', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000006, 1, NULL, '2019-03-05 17:31:22', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0');
/*!40000 ALTER TABLE `USER_ACTIVITY` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USER_ROLE
CREATE TABLE IF NOT EXISTS `USER_ROLE` (
  `USER_ID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USER_ROLE: ~4 rows (approximately)
/*!40000 ALTER TABLE `USER_ROLE` DISABLE KEYS */;
INSERT INTO `USER_ROLE` (`USER_ID`, `ROLE_ID`) VALUES
	(1, 0),
	(2, 1),
	(3, 2),
	(4, 3);
/*!40000 ALTER TABLE `USER_ROLE` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.WALI_KELAS
CREATE TABLE IF NOT EXISTS `WALI_KELAS` (
  `ID_WALIKELAS` varchar(255) NOT NULL,
  `KELAS` varchar(255) DEFAULT NULL,
  `TAHUN_AJARAN` varchar(255) DEFAULT NULL,
  `ID_GURU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_WALIKELAS`),
  KEY `FK841uiexu5avfvxan4igkqixox` (`ID_GURU`),
  CONSTRAINT `FK841uiexu5avfvxan4igkqixox` FOREIGN KEY (`ID_GURU`) REFERENCES `GURU` (`ID_GURU2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.WALI_KELAS: ~0 rows (approximately)
/*!40000 ALTER TABLE `WALI_KELAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WALI_KELAS` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


CREATE TABLE IF NOT EXISTS `SISWA` (
  `ID` varchar(50) DEFAULT NULL,
  `NIS` int(11) DEFAULT NULL,
  `NISN` int(11) DEFAULT NULL,
  `NAMA` varchar(50) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.SISWA: ~16 rows (approximately)
/*!40000 ALTER TABLE `SISWA` DISABLE KEYS */;
INSERT INTO `SISWA` (`ID`, `NIS`, `NISN`, `NAMA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `AGAMA`, `TELEPHONE`) VALUES
	('1', 1111111, 1111112, 'Dika 1 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('2', 1111111, 1111112, 'Dika 2 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('3', 1111111, 1111112, 'Dika 3 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('4', 1111111, 1111112, 'Dika 4 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('5', 1111111, 1111112, 'Dika 5 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('6', 1111111, 1111112, 'Dika 6 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('7', 1111111, 1111112, 'Dika 7 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('11', 1111111, 1111112, 'Dika 11 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('12', 1111111, 1111112, 'Dika 12 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('13', 1111111, 1111112, 'Dika 13 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('14', 1111111, 1111112, 'Dika 14 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('15', 1111111, 1111112, 'Dika 15 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('16', 1111111, 1111112, 'Dika 16 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('17', 1111111, 1111112, 'Dika 17 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('18', 1111111, 1111112, 'Dika 18 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111'),
	('19', 1111111, 1111112, 'Dika 19 Saputra', 'Bekasi', '1990-10-05', 'ISLAM', '08771111111');
/*!40000 ALTER TABLE `SISWA` ENABLE KEYS */;
