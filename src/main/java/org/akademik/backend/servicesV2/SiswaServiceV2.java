package org.akademik.backend.servicesV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.Siswa;

public interface SiswaServiceV2 {
	void insertSiswa(Siswa s);

	public List<Siswa> getListSiswa(Siswa s);

	public Optional<Siswa> getSiswaById(Siswa s);

	public void deleteSiswa(Siswa s);

	public Siswa updateSiswa(Siswa s);

	public ArrayList<Siswa> findByNameOrId(Siswa s);
	
	
	
	
}
