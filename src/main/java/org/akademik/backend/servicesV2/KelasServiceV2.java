package org.akademik.backend.servicesV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.Kelas;

public interface KelasServiceV2 {
	void insertKelas(Kelas s);

	public List<Kelas> getListKelas(Kelas s);

	public Optional<Kelas> getKelasById(Kelas s);

	public void deleteKelas(Kelas s);

	public Kelas updateKelas(Kelas s);

	public ArrayList<Kelas> findByNameOrId(Kelas s);
	
	
	
	
}
