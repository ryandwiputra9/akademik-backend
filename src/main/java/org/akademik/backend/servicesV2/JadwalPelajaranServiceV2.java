package org.akademik.backend.servicesV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.JadwalPelajaran;

public interface JadwalPelajaranServiceV2 {
	void insertJadwalPelajaran(JadwalPelajaran s);

	public List<JadwalPelajaran> getListJadwalPelajaran(JadwalPelajaran s);

	public Optional<JadwalPelajaran> getJadwalPelajaranById(JadwalPelajaran s);

	public void deleteJadwalPelajaran(JadwalPelajaran s);

	public JadwalPelajaran updateJadwalPelajaran(JadwalPelajaran s);

	public ArrayList<JadwalPelajaran> findByNameOrId(JadwalPelajaran s);
	
	
	
	
}
