package org.akademik.backend.servicesV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.PembagianKelas;

public interface PembagianKelasServiceV2 {
	void insertPembagianKelas(PembagianKelas s);

	public List<PembagianKelas> getListPembagianKelas(PembagianKelas s);

	public Optional<PembagianKelas> getPembagianKelasById(PembagianKelas s);

	public void deletePembagianKelas(PembagianKelas s);

	public PembagianKelas updatePembagianKelas(PembagianKelas s);

	public ArrayList<PembagianKelas> findByNameOrId(PembagianKelas s);
	
	
	
	
}
