package org.akademik.backend.servicesV2.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.JadwalPelajaran;
import org.akademik.backend.repository.JadwalPelajaranRepository;
import org.akademik.backend.servicesV2.JadwalPelajaranServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JadwalPelajaranServiceV2Impl implements JadwalPelajaranServiceV2  {

	private static final Logger logger = LoggerFactory.getLogger(JadwalPelajaranServiceV2Impl.class);
	
	@Autowired
	private JadwalPelajaranRepository repository;
	
	@Override
	public void insertJadwalPelajaran(JadwalPelajaran s) {
		logger.debug("insert"+s);
		repository.save(s);
		
	}
	@Override
	public List<JadwalPelajaran> getListJadwalPelajaran(JadwalPelajaran s) {
		return repository.findAll();
	}
	@Override
	public Optional<JadwalPelajaran> getJadwalPelajaranById(JadwalPelajaran s) {
		return repository.findById(s.getId());
	}
	@Override
	public void deleteJadwalPelajaran(JadwalPelajaran s) {
		repository.delete(s.getId());//deleteById(s.getId());
		
	}
	@Override
	public JadwalPelajaran updateJadwalPelajaran(JadwalPelajaran s) {
		return repository.saveAndFlush(s);
	}
	@Override
	public ArrayList<JadwalPelajaran> findByNameOrId(JadwalPelajaran s) {
		// TODO Auto-generated method stub
		return null;
	}

}
