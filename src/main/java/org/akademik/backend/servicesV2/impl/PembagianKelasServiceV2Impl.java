package org.akademik.backend.servicesV2.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.PembagianKelas;
import org.akademik.backend.repository.PembagianKelasRepository;
import org.akademik.backend.servicesV2.PembagianKelasServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PembagianKelasServiceV2Impl implements PembagianKelasServiceV2  {

	private static final Logger logger = LoggerFactory.getLogger(PembagianKelasServiceV2Impl.class);
	
	@Autowired
	private PembagianKelasRepository repository;
	
	@Override
	public void insertPembagianKelas(PembagianKelas s) {
		logger.debug("insert"+s);
		repository.save(s);
		
	}
	@Override
	public List<PembagianKelas> getListPembagianKelas(PembagianKelas s) {
		return repository.findAll();
	}
	@Override
	public Optional<PembagianKelas> getPembagianKelasById(PembagianKelas s) {
		return  repository.findById(s.getId());
	}
	@Override
	public void deletePembagianKelas(PembagianKelas s) {
		repository.delete(s.getId());//deleteById(s.getId());
		
	}
	@Override
	public PembagianKelas updatePembagianKelas(PembagianKelas s) {
		return repository.saveAndFlush(s);
	}
	@Override
	public ArrayList<PembagianKelas> findByNameOrId(PembagianKelas s) {
		// TODO Auto-generated method stub
		return null;
	}

}
