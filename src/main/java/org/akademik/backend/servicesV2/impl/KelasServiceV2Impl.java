package org.akademik.backend.servicesV2.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.Kelas;
import org.akademik.backend.repository.KelasRepository;
import org.akademik.backend.servicesV2.KelasServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KelasServiceV2Impl implements KelasServiceV2  {

	private static final Logger logger = LoggerFactory.getLogger(KelasServiceV2Impl.class);
	
	@Autowired
	private KelasRepository repository;
	
	@Override
	public void insertKelas(Kelas s) {
		logger.debug("insert"+s);
		repository.save(s);
		
	}
	@Override
	public List<Kelas> getListKelas(Kelas s) {
		return repository.findAll();
	}
	@Override
	public Optional<Kelas> getKelasById(Kelas s) {
		return  repository.findById(s.getId());
	}
	@Override
	public void deleteKelas(Kelas s) {
		repository.delete(s.getId());//deleteById(s.getId());
		
	}
	@Override
	public Kelas updateKelas(Kelas s) {
		return repository.saveAndFlush(s);
	}
	@Override
	public ArrayList<Kelas> findByNameOrId(Kelas s) {
		// TODO Auto-generated method stub
		return null;
	}

}
