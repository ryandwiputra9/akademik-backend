package org.akademik.backend.servicesV2.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.Siswa;
import org.akademik.backend.repository.SiswaRepository;
import org.akademik.backend.servicesV2.SiswaServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SiswaServiceV2Impl implements SiswaServiceV2  {

	private static final Logger logger = LoggerFactory.getLogger(SiswaServiceV2Impl.class);
	
	@Autowired
	private SiswaRepository repository;
	
	@Override
	public void insertSiswa(Siswa s) {
		logger.debug("insert"+s);
		repository.save(s);
		
	}
	@Override
	public List<Siswa> getListSiswa(Siswa s) {
		return repository.findAll();
	}
	@Override
	public Optional<Siswa> getSiswaById(Siswa s) {
		return  repository.findById(s.getId());
	}
	@Override
	public void deleteSiswa(Siswa s) {
		repository.delete(s.getId());//deleteById(s.getId());
		
	}
	@Override
	public Siswa updateSiswa(Siswa s) {
		return repository.saveAndFlush(s);
	}
	@Override
	public ArrayList<Siswa> findByNameOrId(Siswa s) {
		// TODO Auto-generated method stub
		return null;
	}

}
