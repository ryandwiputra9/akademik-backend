package org.akademik.backend.servicesV2.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.Guru;
import org.akademik.backend.repository.GuruRepository;
import org.akademik.backend.servicesV2.GuruServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuruServiceV2Impl implements GuruServiceV2 {

	private static final Logger logger = LoggerFactory.getLogger(GuruServiceV2Impl.class);

	@Autowired
	private GuruRepository repository;

	@Override
	public void insertGuru(Guru s) {
		logger.debug("insert" + s);
		repository.save(s);

	}
	@Override
	public void deleteGuru(Guru s) {
		logger.debug("delete" + s);
		repository.delete(s.getId());

	}
	@Override
	public ArrayList<Guru> findByNameOrId(Guru s) {
		return null;
	}
	@Override
	public List<Guru> getListGuru() {
		return repository.findAll();
	}
	@Override
	public Optional<Guru> findGuruById(Guru s) {
		logger.debug("findGuruById" + s);
		return repository.findById(s.getId());
	}
	@Override
	public void updateGuru(Guru s) {
		logger.debug("update" + s);
		repository.saveAndFlush(s);
	}

}
