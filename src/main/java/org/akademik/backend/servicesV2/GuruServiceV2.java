package org.akademik.backend.servicesV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.akademik.backend.entity.Guru;

public interface GuruServiceV2 {
	public void insertGuru(Guru s);

	public List<Guru> getListGuru();

	public Optional<Guru> findGuruById(Guru s);

	public void deleteGuru(Guru s);

	public void updateGuru(Guru s);

	public ArrayList<Guru> findByNameOrId(Guru s);
	
	
	
	
}
