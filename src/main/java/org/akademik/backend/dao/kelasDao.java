package org.akademik.backend.dao;

import java.util.List;

import org.akademik.backend.entity.Kelas;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface kelasDao {
	public void insertKelas(Kelas kls);
	public Kelas updateKelas(Kelas kls);
	public void deleteKelas(Kelas kls);
	
	public Kelas findKelasById(String id);
	public List<Kelas> findAllKelas(Kelas kls);
	//public ArrayList<Siswa> findByNameOrId(Siswa s);
}
