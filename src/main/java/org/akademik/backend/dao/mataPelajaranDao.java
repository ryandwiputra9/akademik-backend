package org.akademik.backend.dao;

import java.util.List;

import org.akademik.backend.entity.MataPelajaran;
import org.akademik.backend.entity.Siswa;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface mataPelajaranDao {
	public void insertMataPelajaran(MataPelajaran mp);

	public MataPelajaran updateMataPelajaran(MataPelajaran mp);

	public void deleteMataPelajaran(MataPelajaran mp);

	public List<MataPelajaran> getAllMataPelajaran(MataPelajaran mp);

}
