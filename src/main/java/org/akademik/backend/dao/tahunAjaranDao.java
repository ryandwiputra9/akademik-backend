package org.akademik.backend.dao;

import java.util.List;

import org.akademik.backend.entity.Siswa;
import org.akademik.backend.entity.TahunAjaran;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface tahunAjaranDao {
	public void insertThAjaran(TahunAjaran th);
	public TahunAjaran updateThAjaran(TahunAjaran th);
	public void deleteThAjaran(TahunAjaran th);
	
	//public Siswa findSiswaById(String id);
	public List<TahunAjaran> getAllThAjaran();
	//public ArrayList<Siswa> findByNameOrId(Siswa s);
}
