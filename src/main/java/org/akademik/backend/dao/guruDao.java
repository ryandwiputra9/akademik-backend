package org.akademik.backend.dao;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.Guru;
import org.apache.ibatis.annotations.Mapper;
@Mapper
public interface guruDao {
	public void insertGuru(Guru user);

	public void updateGuru(Guru user);

	public void deleteGuru(Guru user);

	public Guru findGuruById(String id);

	public ArrayList<Guru> findByNameOrId(Guru s);

	public List<Guru> findAllGuru();
}
