package org.akademik.backend.dao;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.Walikelas;
import org.apache.ibatis.annotations.Mapper;
@Mapper
public interface walikelasDao {
	public void insertWalikelas(Walikelas wk);

	public void updateWalikelas(Walikelas wk);

	public void deleteWalikelas(Walikelas wk);

	public Walikelas findWalikelasById(Walikelas id);

	public List<Walikelas> findAllWalikelas();

	public ArrayList<Walikelas> getListByNameOrId(Walikelas s);
}
