package org.akademik.backend.dao;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.PembagianKelas;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface pembagianKelasDao {
	void insertPembagianKelas(PembagianKelas s);

	public List<PembagianKelas> getListPembagianKelas(PembagianKelas s);

	/*public List<PembagianKelas> getPembagianKelasById(PembagianKelas s);*/

	public void deletePembagianKelas(PembagianKelas s);

	public PembagianKelas updatePembagianKelas(PembagianKelas s);

	public ArrayList<PembagianKelas> findByNameOrId(PembagianKelas s);
}
