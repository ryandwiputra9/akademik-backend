package org.akademik.backend.dao;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.ActivityLog;
import org.akademik.backend.entity.Siswa;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface activityDao {
	public void insertActivity(ActivityLog log);
	public List<ActivityLog> checkAlluserActivity();

}
