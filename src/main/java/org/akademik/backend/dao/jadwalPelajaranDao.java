package org.akademik.backend.dao;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.JadwalPelajaran;
import org.akademik.backend.entity.Siswa;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface jadwalPelajaranDao {
	public void insertJadwalPelajaran(JadwalPelajaran jp);
	public JadwalPelajaran updateJadwalPelajaran(JadwalPelajaran jp);
	public void deleteJadwalPelajaran(JadwalPelajaran jp);
	
	//public JadwalPelajaran findJadwalPelajaranById(String id);
	public List<JadwalPelajaran> getAllJadwalPelajaran(JadwalPelajaran jp);
	//public ArrayList<Siswa> findByNameOrId(JadwalPelajaran jp);
}
