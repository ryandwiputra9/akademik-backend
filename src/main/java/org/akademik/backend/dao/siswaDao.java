package org.akademik.backend.dao;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.Siswa;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface siswaDao {
	public void insertSiswa(Siswa user);
	public Siswa updateSiswa(Siswa user);
	public void deleteSiswa(Siswa user);
	
	public Siswa findSiswaById(String id);
	public List<Siswa> findAllSiswa(Siswa s);
	public ArrayList<Siswa> findByNameOrId(Siswa s);
}
