package org.akademik.backend.repository;

import java.util.Optional;

import org.akademik.backend.entity.Guru;
import org.akademik.backend.entity.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KelasRepository extends JpaRepository<Kelas, String> {
	Optional<Kelas> findById(String id);

}
