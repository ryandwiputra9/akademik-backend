package org.akademik.backend.repository;

import java.util.Optional;

import org.akademik.backend.entity.JadwalPelajaran;
import org.akademik.backend.entity.PembagianKelas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PembagianKelasRepository extends JpaRepository<PembagianKelas, String> {
	Optional<PembagianKelas> findById(String id);
}
