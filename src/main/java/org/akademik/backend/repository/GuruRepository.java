package org.akademik.backend.repository;

import java.util.Optional;

import org.akademik.backend.entity.Guru;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuruRepository extends JpaRepository<Guru, String> {

	Optional<Guru> findById(String id);

/*	@Modifying
	@Query("update SISWA s set s.NIS = ?,s.NISN=?,s., u.lastname = ?2 where u.id = ?3")
	void update(Siswa s);*/
	
}
