package org.akademik.backend.repository;

import org.akademik.backend.entity.TahunAjaran;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TahunAJaranRepository extends JpaRepository<TahunAjaran, String> {

}
