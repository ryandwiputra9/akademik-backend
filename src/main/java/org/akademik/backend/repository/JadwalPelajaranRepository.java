package org.akademik.backend.repository;

import java.util.Optional;

import org.akademik.backend.entity.JadwalPelajaran;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JadwalPelajaranRepository extends JpaRepository<JadwalPelajaran, String> {
	Optional<JadwalPelajaran> findById(String id);
}
