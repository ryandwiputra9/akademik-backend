package org.akademik.backend.repository;

import java.util.Optional;

import org.akademik.backend.entity.Siswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SiswaRepository extends JpaRepository<Siswa, String> {

	Optional<Siswa> findById(String id);

/*	@Modifying
	@Query("update SISWA s set s.NIS = ?,s.NISN=?,s., u.lastname = ?2 where u.id = ?3")
	void update(Siswa s);*/
	
}
