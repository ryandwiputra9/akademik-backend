package org.akademik.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.dao.activityDao;
import org.akademik.backend.dao.siswaDao;
import org.akademik.backend.entity.ActivityLog;
import org.akademik.backend.entity.Siswa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {
/*	@Autowired
	CustomUserDetailsService userDetail;*/

	@Autowired
	private activityDao dao;
	private static final Logger logger = LoggerFactory.getLogger(ActivityService.class);

	public List<ActivityLog> getAllActivityUser() {

		List<ActivityLog> list = dao.checkAlluserActivity();

		return list;
	}

	/*
	 * public ActivityLog getUserActivity(ActivityLog s) { ActivityLog act =
	 * dao.getUserActivity(s.getUserId()); return act; }
	 * 
	 * 
	 * 
	 * public ArrayList<Siswa> findByNameOrId(Siswa s) { ArrayList<Siswa> list =
	 * dao.findByNameOrId(s); return list; }
	 */
}
