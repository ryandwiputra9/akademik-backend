package org.akademik.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.Guru;

public interface GuruService {
	
	public void insertGuru(Guru s);

	public List<Guru> getListGuru();

	public Guru findGuruById(Guru s);

	public void deleteGuru(Guru s);

	public void updateGuru(Guru s);

	public ArrayList<Guru> findByNameOrId(Guru s);
}
