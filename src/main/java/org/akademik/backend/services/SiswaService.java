package org.akademik.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.Siswa;

public interface SiswaService {
	void insertSiswa(Siswa s);

	public List<Siswa> getListSiswa(Siswa s);

	public Siswa getSiswaById(Siswa s);

	public void deleteSiswa(Siswa s);

	public Siswa updateSiswa(Siswa s);

	public ArrayList<Siswa> findByNameOrId(Siswa s);
	
	
	
	
}
