package org.akademik.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.entity.PembagianKelas;

public interface PembagianKelasService {
	void insertPembagianKelas(PembagianKelas s);

	public List<PembagianKelas> getListPembagianKelas(PembagianKelas s);

	public List<PembagianKelas> getPembagianKelasById(PembagianKelas s);

	public void deletePembagianKelas(PembagianKelas s);

	public PembagianKelas updatePembagianKelas(PembagianKelas s);

	public ArrayList<PembagianKelas> findByNameOrId(PembagianKelas s);

}
