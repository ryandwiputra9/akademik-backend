package org.akademik.backend.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.dao.guruDao;
import org.akademik.backend.entity.Guru;
import org.akademik.backend.services.GuruService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuruServiceImpl implements GuruService {

	@Autowired
	private guruDao daoGuru;

	private static final Logger logger = LoggerFactory.getLogger(GuruServiceImpl.class);
	@Override
	public void insertGuru(Guru g) {
		daoGuru.insertGuru(g);
	}
	@Override
	public List<Guru> getListGuru() {
		List<Guru> list = daoGuru.findAllGuru();
		return list;
	}
	@Override
	public Guru findGuruById(Guru s) {
		Guru Guru = daoGuru.findGuruById(s.getId());
		return Guru;
	}
	@Override
	public ArrayList<Guru> findByNameOrId(Guru s) {
		ArrayList<Guru> list = daoGuru.findByNameOrId(s);
		return list;
	}
	@Override
	public void updateGuru(Guru s) {
		daoGuru.updateGuru(s);
	}
	@Override
	public void deleteGuru(Guru s) {
		daoGuru.deleteGuru(s);

	}
}
