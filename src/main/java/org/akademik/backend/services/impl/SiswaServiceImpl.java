package org.akademik.backend.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.dao.siswaDao;
import org.akademik.backend.entity.Siswa;
import org.akademik.backend.services.SiswaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SiswaServiceImpl implements SiswaService {
	/*
	 * @Autowired CustomUserDetailsService userDetail;
	 */

	@Autowired
	private siswaDao dao;
	private static final Logger logger = LoggerFactory.getLogger(SiswaServiceImpl.class);

	@Override
	public void insertSiswa(Siswa s) {
		dao.insertSiswa(s);
	}
	@Override
	public List<Siswa> getListSiswa(Siswa s) {
		List<Siswa> list = dao.findAllSiswa(s);
		return list;
	}
	@Override
	public Siswa getSiswaById(Siswa s) {
		Siswa siswa = dao.findSiswaById(s.getId());
		return siswa;
	}
	@Override
	public void deleteSiswa(Siswa s) {
		dao.deleteSiswa(s);

	}
	@Override
	public Siswa updateSiswa(Siswa s) {
		
		return dao.updateSiswa(s);
	}
	@Override
	public ArrayList<Siswa> findByNameOrId(Siswa s) {
		ArrayList<Siswa> list = dao.findByNameOrId(s);
		return list;
	}
}
