package org.akademik.backend.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.akademik.backend.dao.pembagianKelasDao;
import org.akademik.backend.entity.PembagianKelas;
import org.akademik.backend.services.PembagianKelasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PembagianKelasServiceImpl implements PembagianKelasService {

	private static final Logger logger = LoggerFactory.getLogger(PembagianKelasServiceImpl.class);

	@Autowired
	private pembagianKelasDao Dao;

	@Override
	public void insertPembagianKelas(PembagianKelas s) {
		logger.debug("insert" + s);
		Dao.insertPembagianKelas(s);

	}

	@Override
	public List<PembagianKelas> getListPembagianKelas(PembagianKelas s) {
		//logger.debug("getListPembagianKelas");
		List<PembagianKelas> list = null;
		try {
			 list = Dao.getListPembagianKelas( s);
		}catch(Exception e) {
			logger.error("errorrr"+e);
		}
		return list;
	}

	@Override
	public List<PembagianKelas> getPembagianKelasById(PembagianKelas s) {
		logger.debug("getListPembagianKelasById");
		List<PembagianKelas> list = Dao.getListPembagianKelas(s);
		return list;
	}

	@Override
	public void deletePembagianKelas(PembagianKelas s) {
		logger.debug("deletePembagianKelas");
		Dao.deletePembagianKelas(s);

	}

	@Override
	public PembagianKelas updatePembagianKelas(PembagianKelas s) {
		logger.debug("updatePembagianKelas");
		return Dao.updatePembagianKelas(s);
	}

	@Override
	public ArrayList<PembagianKelas> findByNameOrId(PembagianKelas s) {
		// TODO Auto-generated method stub
		return null;
	}

}
