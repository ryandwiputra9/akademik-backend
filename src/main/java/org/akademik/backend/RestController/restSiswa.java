package org.akademik.backend.RestController;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.akademik.backend.entity.Siswa;
import org.akademik.backend.services.SiswaService;
import org.akademik.backend.servicesV2.SiswaServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class restSiswa{/* extends UserDetails{*/
//	 private final Logger logger = Logger.getLogger(getClass().getName());
	// private final Logger logger = Logger.getLogger(getClass().getName());
//	private final Logger logger = Logger.getLogger(this.getClass().getName());
//	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private SiswaServiceV2 serviceV2;
	@Autowired
	private SiswaService service;
	
	
	private Siswa siswa =null;

/*	//@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Siswa>> getAll() {
		List<Siswa> Siswa = service.getListSiswa();
		if (!Siswa.isEmpty()) {
			return new ResponseEntity<List<Siswa>>(Siswa, HttpStatus.OK);//
			// ResponseEntity.ok(products);
		}
		logger.info("Logger CUY {} ");
		System.out.println("USER ID : "+getUser().getId());
		System.out.println("USER Name : "+getUser().getUsername());
		logger.info("USER Name : {}",getUser().getUsername());
	//	logger.debug("Logger ALL SISWA : " + Siswa);
		logger.debug("Logger ALL SISWA : {}" , Siswa);
		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Siswa);
	}

//	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Siswa>> searchByNameOrId(@RequestParam(required = false) String id,
			@RequestParam(required = false) String nama) {
		logger.info("Search  ID :" + id + " Nama : " + nama);
		Siswa s = new Siswa();
		s.setId(id);
		s.setNama(nama);
		List<Siswa> Siswa = service.findByNameOrId(s);

		if (!Siswa.isEmpty())
			return new ResponseEntity<List<Siswa>>(Siswa, HttpStatus.OK);//
		else
			return new ResponseEntity<List<Siswa>>(Siswa, HttpStatus.NO_CONTENT);//

	}

	
	 * Note jika dengan @PathVariable localhost:9999/api/siswa/1 jika
	 * dengan @RequestParam localhost:9999/api/siswa/?id=1
	 * 
	 

	// @GetMapping(value = "/siswa/{id}") with pathVariable
//	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/") // localhost:9999/api/siswa/?id=1
	public HttpEntity<Siswa> getById(// @PathVariable("id") String id
			@RequestParam(required = false) String id

	) {

		logger.info("getById :" + id);
		Siswa input = new Siswa();
		input.setId(id);
		Siswa siswa = service.getSiswaById(input);
		if (siswa == null) {
			return new ResponseEntity<Siswa>(siswa, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(siswa);
	}

//	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@PostMapping(value = "/siswa")
	@ResponseStatus(HttpStatus.CREATED)
	public void AddSiswa(@RequestBody Siswa s) {
		logger.info("Add SISWA : " + s);
		service.insertSiswa(s);
	}

//	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@PutMapping(value = "/siswa")
	@ResponseStatus(HttpStatus.OK)
	public List<Siswa> UpdateSiswa(@RequestBody Siswa s) {
		logger.info("Update SISWA : " + s);

		 reload List all siswa 
		List<Siswa> list = service.updateSiswa(s);
		return list;
	}

//	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@DeleteMapping(value = "/siswa")
	@ResponseStatus(HttpStatus.OK)
	public void DeleteSiswa(@RequestBody Siswa s) {
		logger.info("Delete SISWA : " + s);
		service.deleteSiswa(s);
	}

	
	 * @GetMapping(value = "/siswa/xml/", produces = {
	 * MediaType.APPLICATION_XML_VALUE }) public ResponseEntity<List<Siswa>> xml() {
	 * List<Siswa> products = service.getListSiswa(); logger.info("<<<Loginf>>>");
	 * logger.info("Logginf : " + products); return new
	 * ResponseEntity<List<Siswa>>(products, HttpStatus.OK); }
	 
//	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/xml/", produces = { MediaType.APPLICATION_XML_VALUE }) // localhost:9999/api/siswa/?id=1
	public HttpEntity<ArrayList<Siswa>> getByIdXml(// @PathVariable("id") String id
			@RequestParam(required = false) String id, @RequestParam(required = false) String nama) {

		logger.info("Search  ID :" + id + " Nama : " + nama);
		Siswa input = new Siswa();
		input.setId(id);
		input.setNama(nama);
		ArrayList<Siswa> siswa = service.findByNameOrId(input);
		if (siswa.isEmpty()) {
			return new ResponseEntity<ArrayList<Siswa>>(siswa, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}
		return new HttpEntity<>(siswa);
	}*/
	
	
	  /*API v1*/
	
	/*with @RestController*/
	@RequestMapping(value="/v1/siswa/", method = RequestMethod.GET)
    public List<Siswa> cariSiswa(@PathVariable(required = false) String id){
		siswa=new Siswa();
		siswa.setId(id);
    	return service.getListSiswa(siswa);
    }
    
    @RequestMapping(value="/v1/siswa/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void insertSiswaBaru(@RequestBody Siswa s){
    	logger.info("==Response==\n "+s.toString());
    	service.insertSiswa(s);
    }
    
    @RequestMapping(value="/v1/siswa/", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Siswa updatePeserta(@RequestBody Siswa p){
    	logger.debug(">>>>>updatePeserta>>>>>>>" + p.toString());
    	siswa=service.updateSiswa(p);
    	return siswa;
    }
    
	@RequestMapping(value = "/v1/siswa/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Siswa getSiswaById(@PathVariable("id") String id) {// SearchData searchData){
		logger.debug(">>>>>getSiswaById>>>>>>>" + id);
		siswa = new Siswa();
		siswa.setId(id);

		return service.getSiswaById(siswa);
	}
    
    @RequestMapping(value="/v1/siswa/", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void hapusPeserta(@RequestBody Siswa p){
    	logger.debug(">>>>>>delete>>>>>>" + p.toString());
    	service.deleteSiswa(p);
    }

	
    
    /*API v2*/
    
	@GetMapping("/v2/siswa/")
	public ResponseEntity<List<Siswa>> findAll(@PathVariable(required = false) String id) {
		siswa = new Siswa();
		List<Siswa> x = serviceV2.getListSiswa(siswa);
		return ResponseEntity.ok(x);
	}

	@GetMapping("/v2/siswa/{id}")
	public ResponseEntity<Siswa> findById(@PathVariable String id) {
		siswa = new Siswa();
		siswa.setId(id);
		Optional<Siswa> siswaOptional = serviceV2.getSiswaById(siswa);
		if (!siswaOptional.isPresent()) {
			return new ResponseEntity("ID Tidak Ditemukan", HttpStatus.NO_CONTENT);
			//return new ResponseEntity.badRequest().body("ID tidak Terdaftar");
		}

		return ResponseEntity.ok(siswaOptional.get());
	}

	@PostMapping("/v2/siswa/")
	public ResponseEntity<Siswa> create(@Valid @RequestBody Siswa siswa) {

		serviceV2.insertSiswa(siswa);

		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/v2/siswa/{id}")
	public ResponseEntity<Siswa> update(@PathVariable String id, @RequestBody Siswa siswa) {
		siswa.setId(id);
		if (!serviceV2.getSiswaById(siswa).isPresent()) {
			return new ResponseEntity("ID tidak Terdaftar", HttpStatus.BAD_REQUEST);
		}

		siswa = serviceV2.updateSiswa(siswa);

		return ResponseEntity.accepted().body(siswa);
	}

	@DeleteMapping("/v2/siswa/{id}")
	public ResponseEntity delete(@PathVariable String id) {
		siswa = new Siswa();
		siswa.setId(id);
		
		if (serviceV2.getSiswaById(siswa).isPresent() == false) {
			logger.debug("ID not Found");

			/*
			 * return new ResponseEntity<>( "ID tidak Terdaftar", HttpStatus.BAD_REQUEST);
			 */
			//return ResponseEntity.badRequest().body("ID tidak Terdaftar");
			return ResponseEntity.badRequest().header("Error", "ID Tidak Terdaftar")
					.body("ID Tidak Terdaftar 2");
			
		}

		serviceV2.deleteSiswa(siswa);

		return ResponseEntity.accepted().build();
	}
}
