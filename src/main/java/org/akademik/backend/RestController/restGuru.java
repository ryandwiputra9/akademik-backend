package org.akademik.backend.RestController;

import java.util.List;
import java.util.logging.Logger;

import org.akademik.backend.entity.Guru;
import org.akademik.backend.services.impl.GuruServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class restGuru {

	private final Logger logger = Logger.getLogger(getClass().getName());
	@Autowired
	private GuruServiceImpl guruService;

	@GetMapping(value = "/v1/guru", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Guru>> getAll() {
		List<Guru> guru = guruService.getListGuru();
		if (!guru.isEmpty()) {
			return new ResponseEntity<List<Guru>>(guru, HttpStatus.OK);//
			// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(guru);
	}
	@GetMapping(value = "/v1/guru/", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Guru>> searchByNameOrId(@RequestParam(required = false) String id,
			@RequestParam(required = false) String nama) {
		logger.info("Search  ID :" + id + " Nama : " + nama);
		Guru s = new Guru();
		s.setId(id);
		s.setNama(nama);
		List<Guru> Guru = guruService.findByNameOrId(s);

		if (!Guru.isEmpty())
			return new ResponseEntity<List<Guru>>(Guru, HttpStatus.OK);//
		else
			return new ResponseEntity<List<Guru>>(Guru, HttpStatus.NO_CONTENT);//

	}

	/*
	 * Note jika dengan @PathVariable localhost:9999/api/Guru/1 jika
	 * dengan @RequestParam localhost:9999/api/Guru/?id=1
	 * 
	 */

	// @GetMapping(value = "/Guru/{id}") with pathVariable
	@GetMapping(value = "/v1/guru/") // localhost:9999/api/Guru/?id=1
	public HttpEntity<Guru> getById(// @PathVariable("id") String id
			@RequestParam(required = false) String id

	) {

		logger.info("getById :" + id);
		Guru input = new Guru();
		input.setId(id);
		Guru Guru = guruService.findGuruById(input);
		if (Guru == null) {
			return new ResponseEntity<Guru>(Guru, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Guru);
	}

	@PostMapping(value = "/v1/guru/")
	@ResponseStatus(HttpStatus.CREATED)
	public void AddGuru(@RequestBody Guru s) {
		logger.info("Add Guru : " + s);
		guruService.insertGuru(s);
	}

	@PutMapping(value = "/v1/guru/")
	@ResponseStatus(HttpStatus.OK)
	public void UpdateGuru(@RequestBody Guru s) {
		logger.info("Update Guru : " + s);

		guruService.updateGuru(s);

	}

	@DeleteMapping(value = "/v1/guru/")
	@ResponseStatus(HttpStatus.OK)
	public void DeleteGuru(@RequestBody Guru s) {
		logger.info("Delete Guru : " + s);
		guruService.deleteGuru(s);
	}

	

}
