package org.akademik.backend.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.akademik.backend.entity.Walikelas;
import org.akademik.backend.services.impl.WalikelasServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class restWalikelas {

	private final Logger logger = Logger.getLogger(getClass().getName());
	@Autowired
	private WalikelasServiceImpl service;

	@GetMapping(value = "/v1/walikelas", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Walikelas>> getAll() {
		List<Walikelas> Walikelas = service.getListWalikelas();
		if (!Walikelas.isEmpty()) {
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.OK);//
			// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Walikelas);
	}
	@GetMapping(value = "/v1/dummy/walikelas", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Walikelas>> getDummyWalikelas() {
		List<Walikelas> Walikelas =null; //service.getListWalikelasDummy();
		if (!Walikelas.isEmpty()) {
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.OK);//
		}
		return new HttpEntity<>(Walikelas);
	}

	@GetMapping(value = "/v1/walikelas/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Walikelas>> searchByNameOrId(@RequestParam(required = false) String id,
			@RequestParam(required = false) String nama) {
		logger.info("Search  ID :" + id + " Nama : " + nama);
		Walikelas s = new Walikelas();
		s.setIdWalikelas(id);
		//s.setNama(nama);
		List<Walikelas> Walikelas = service.getListByNameOrId(s);

		if (!Walikelas.isEmpty())
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.OK);//
		else
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.NO_CONTENT);//

	}

	/*
	 * Note jika dengan @PathVariable localhost:9999/api/Walikelas/1 jika
	 * dengan @RequestParam localhost:9999/api/Walikelas/?id=1
	 * 
	 */

	// @GetMapping(value = "/Walikelas/{id}") with pathVariable
	@GetMapping(value = "/v1/walikelas/") // localhost:9999/api/Walikelas/?id=1
	public HttpEntity<Walikelas> getById(// @PathVariable("id") String id
			@RequestParam(required = false) String id

	) {

		logger.info("getById :" + id);
		Walikelas input = new Walikelas();
		input.setIdWalikelas(id);
		Walikelas Walikelas = service.getWalikelasById(input);
		if (Walikelas == null) {
			return new ResponseEntity<Walikelas>(Walikelas, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Walikelas);
	}

	@PostMapping(value = "/v1/walikelas")
	@ResponseStatus(HttpStatus.CREATED)
	public void AddWalikelas(@RequestBody Walikelas s) {
		logger.info("Add Walikelas : " + s);
		service.insertWalikelas(s);
	}

	@PutMapping(value = "/v1/walikelas")
	@ResponseStatus(HttpStatus.OK)
	public List<Walikelas> UpdateWalikelas(@RequestBody Walikelas s) {
		logger.info("Update Walikelas : " + s);

		/* reload List all Walikelas */
		List<Walikelas> list=null;// = service.updateWalikelas(s);
		return list;
	}

	@DeleteMapping(value = "/v1/walikelas")
	@ResponseStatus(HttpStatus.OK)
	public void DeleteWalikelas(@RequestBody Walikelas s) {
		logger.info("Delete Walikelas : " + s);
		service.deleteWalikelas(s);
	}

	@GetMapping(value = "/v1/walikelas/xml/", produces = { MediaType.APPLICATION_XML_VALUE }) // localhost:9999/api/Walikelas/?id=1
	public HttpEntity<ArrayList<Walikelas>> getByIdXml(// @PathVariable("id") String id
			@RequestParam(required = false) String id, @RequestParam(required = false) String nama) {

		logger.info("Search  ID :" + id + " Nama : " + nama);
		Walikelas input = new Walikelas();
		input.setIdWalikelas(id);
	//	input.setNama(nama);
		ArrayList<Walikelas> Walikelas = service.getListByNameOrId(input);
		if (Walikelas.isEmpty()) {
			return new ResponseEntity<ArrayList<Walikelas>>(Walikelas, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}
		return new HttpEntity<>(Walikelas);
	}

}
