package org.akademik.backend.RestController;

import java.util.List;
import java.util.logging.Logger;

import org.akademik.backend.entity.Kelas;
import org.akademik.backend.entity.PembagianKelas;
import org.akademik.backend.entity.TahunAjaran;
import org.akademik.backend.services.PembagianKelasService;
import org.akademik.backend.servicesV2.PembagianKelasServiceV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class restPembagianKelas {

	private final Logger logger = Logger.getLogger(getClass().getName());
	@Autowired
	private PembagianKelasService servicePembagianKelas;
	@Autowired
	private PembagianKelasServiceV2 servicePembagianKelasV2;
	
	private PembagianKelas pembagianKelas;
    /*API v2*/
    
	@GetMapping("/v1/pembagiankelas/")
	public ResponseEntity<List<PembagianKelas>> findAll() {
		pembagianKelas = new PembagianKelas();
	/*	Kelas k=new Kelas();
		k.setKelas(kelas);
		TahunAjaran th=new TahunAjaran();
		th.setTahunAjaran(thajaran);
		pembagianKelas.setKelas(k);
		pembagianKelas.setTahunAjaran(th);*/
		
		List<PembagianKelas> x = servicePembagianKelas.getListPembagianKelas(pembagianKelas);
		System.out.println("Listx"+x);
		return ResponseEntity.ok(x);
	}
	
	
	@GetMapping("/v2/pembagiankelas/")
	public ResponseEntity<List<PembagianKelas>> findAllV2(@PathVariable(required = false) String id) {
		pembagianKelas = new PembagianKelas();
		pembagianKelas.setId(id);
		List<PembagianKelas> x = servicePembagianKelasV2.getListPembagianKelas(pembagianKelas);
		
		logger.info("LIST"+x);
		System.out.println("Listx"+x);
		
		return ResponseEntity.ok(x);
	}
	
	

	/*@GetMapping("/v2//{id}")
	public ResponseEntity<PembagianKelas> findById(@PathVariable String id) {
		 = new PembagianKelas();
		.setId(id);
		Optional<PembagianKelas> Optional = serviceV2.getPembagianKelasById();
		if (!Optional.isPresent()) {
			return new ResponseEntity("ID Tidak Ditemukan", HttpStatus.NO_CONTENT);
			//return new ResponseEntity.badRequest().body("ID tidak Terdaftar");
		}

		return ResponseEntity.ok(Optional.get());
	}

	@PostMapping("/v2//")
	public ResponseEntity<PembagianKelas> create(@Valid @RequestBody PembagianKelas ) {

		serviceV2.insertPembagianKelas();

		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/v2//{id}")
	public ResponseEntity<PembagianKelas> update(@PathVariable String id, @RequestBody PembagianKelas ) {
		.setId(id);
		if (!serviceV2.getPembagianKelasById().isPresent()) {
			return new ResponseEntity("ID tidak Terdaftar", HttpStatus.BAD_REQUEST);
		}

		 = serviceV2.updatePembagianKelas();

		return ResponseEntity.accepted().body();
	}

	@DeleteMapping("/v2//{id}")
	public ResponseEntity delete(@PathVariable String id) {
		 = new PembagianKelas();
		.setId(id);
		
		if (serviceV2.getPembagianKelasById().isPresent() == false) {
			logger.debug("ID not Found");

			
			 * return new ResponseEntity<>( "ID tidak Terdaftar", HttpStatus.BAD_REQUEST);
			 
			//return ResponseEntity.badRequest().body("ID tidak Terdaftar");
			return ResponseEntity.badRequest().header("Error", "ID Tidak Terdaftar")
					.body("ID Tidak Terdaftar 2");
			
		}

		serviceV2.deletePembagianKelas();

		return ResponseEntity.accepted().build();
	}
	*/

}
