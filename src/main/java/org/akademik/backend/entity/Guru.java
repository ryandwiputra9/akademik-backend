package org.akademik.backend.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
//@Entity
//@Table(name = "GURU")
@Setter
@Getter

public class Guru {
	@Id
	//@GeneratedValue(generator = "uuid")
   // @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID_GURU", unique = true)
	@Size(max = 5)
	private String id;
	@Size(max = 50)
	@Column(name = "NIP", unique = true)
	private String nip;
	@Size(max = 80)
	@Column(name = "NAMA")
	private String nama;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "TANGGAL_LAHIR")
	private Date tanggalLahir;
	
	@Size(max = 50)
	@Column(name = "TEMPAT_LAHIR")
	private String tempatLahir;
	
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "TANGGAL_DIBUAT")
	private Date tanggalDibuat;
	
	@Size(max = 20)
	@Column(name = "AGAMA")
	private String agama;
	
	@Size(max = 50)
	@Column(name = "TELEPHONE", unique = true)
	private String tlp;
	
	@Size(max = 50)
	@Column(name = "EMAIL", unique = true)
	private String email;
	
	@Size(max = 10)
	@Column(name = "LULUSAN")
	private String lulusan;
	
	@Size(max = 25)
	@Column(name = "JABATAN") // guru,kepala sekolah, dll
	private String jabatan;
	
	@Size(max = 20)
	@Column(name = "STATUS_GURU") 
	private String statusGuru; // pns, honorer
	
	
	@Size(max = 1)
	@Column(name = "STATUS")
	private String status;
	
	@OneToMany(mappedBy="guru")//cascade = CascadeType.ALL
	private List <JadwalPelajaran> jadwal;//=new ArrayList<JadwalPelajaran>();
	
	@OneToMany(mappedBy="guru")//cascade = CascadeType.ALL
	private List <PembagianKelas> pembagianKelas;//=new ArrayList<PembagianKelas>();
	
	
}
