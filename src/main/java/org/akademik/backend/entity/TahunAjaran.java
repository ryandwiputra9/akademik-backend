package org.akademik.backend.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
//@Entity
//@Table(name = "TAHUN_AJARAN")
@Setter
@Getter

public class TahunAjaran {
	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TAHUN_AJARAN", unique = true)
	@Size(max = 5)
	private String id;
	@Size(max = 10)
	@Column(name = "TAHUN_AJARAN", unique = true)
	private String tahunAjaran;

	@OneToMany(mappedBy = "tahunAjaran")
	private Set<JadwalPelajaran> jadwal;

}
