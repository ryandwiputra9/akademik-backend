package org.akademik.backend.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
//@Table(name="MATA_PELAJARAN")
//@Entity
public class MataPelajaran {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PELAJARAN", unique = true)
	@Size(max = 5)
	private String idPelajaran;
	
	@Size(max = 80)
	@Column(name = "PELAJARAN", unique = true)
	private String pelajaran;
	
	@OneToMany(mappedBy="pelajaran")
	private Set <JadwalPelajaran> jadwal;
}
