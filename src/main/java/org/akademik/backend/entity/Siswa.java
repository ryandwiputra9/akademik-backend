package org.akademik.backend.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Data

@Getter
@Setter
@Slf4j
//@Table(name = "SISWA")
//@Entity
public class Siswa {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_SISWA", unique = true)
	@Size(max = 5)
	private String id;
	
	@Column(name = "NIS", unique = true)
	@Size(max = 20)
	private String nis;
	
	@Column(name = "NISN", unique = true)
	@Size(max = 20)
	private String nisn;
	
	@Column(name = "NAMA")
	@Size(max = 80)
	private String nama;

	@Temporal(TemporalType.DATE)
	// @DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "TANGGAL_LAHIR")
	private Date tanggalLahir;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TANGGAL_DIBUAT")
	@CreatedDate
	private Date tanggalDibuat;
	
	@Size(max = 50)
	@Column(name = "TEMPAT_LAHIR")
	private String tempatLahir;
	
	@Size(max = 50)
	@Column(name = "TELEPHONE", unique = true)
	private String tlp;
	
	@Size(max = 50)
	@Column(name = "EMAIL", unique = true)
	private String email;
	
	@Size(max = 20)
	@Column(name = "AGAMA")
	private String agama;
	
	@OneToMany(mappedBy="siswa")//cascade = CascadeType.ALL
	// @JsonManagedReference
	private List<PembagianKelas> pembagianKelas;
}
