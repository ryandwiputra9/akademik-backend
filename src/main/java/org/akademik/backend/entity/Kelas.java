package org.akademik.backend.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
//@Table(name="KELAS")
//@Entity
@Data
public class Kelas {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_KELAS", unique = true)
	@Size(max = 5)
	private String id;
	
	@Size(max = 20)
	@Column(name = "KELAS", unique = false)
	private String kelas;
	
	//@Column(name = "ID_TAHUN_AJARAN")
	//private String thAjaran;
	
	//@OneToOne
//	@JoinColumn(name = "ID_TAHUN_AJARAN")
	//private TahunAjaran tahunAjaran;
	/*@OneToMany(mappedBy="kelas")
	private Set <JadwalPelajaran> jadwal;*/
}
