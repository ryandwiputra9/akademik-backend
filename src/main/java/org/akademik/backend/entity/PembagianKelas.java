package org.akademik.backend.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
//@Table(name="PEMBAGIAN_KELAS")
//@Entity
@Data
@Embeddable
public class PembagianKelas {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PEMBAGIAN_KELAS", unique = true)
	@Size(max = 5)
	private String id;
	
	@Size(max = 5)
	@ManyToOne
	@JoinColumn(name = "ID_SISWA")
	 //@JsonBackReference
	private Siswa siswa;
	
	@Size(max = 5)
	@ManyToOne
	@JoinColumn(name = "ID_GURU")
	// @JsonBackReference
	private Guru guru;
	
	@Size(max = 5)
	@ManyToOne
	@JoinColumn(name = "ID_TAHUN_AJARAN")
	private TahunAjaran tahunAjaran;
	
	@Size(max = 5)
	@ManyToOne
	@JoinColumn(name = "ID_KELAS")
	private Kelas kelas;
}
