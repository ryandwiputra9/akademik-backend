package org.akademik.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
//@Entity
//@Table(name = "JADWAL_PELAJARAN")
@Setter
@Getter
public class JadwalPelajaran {
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_JADWAL", unique = true,nullable=false)
	@Size(max = 80)
	private String id;
	
	@Size(max = 10)
	@Column(name = "HARI",nullable=false)
	private String hari;
	/*@Size(max = 6)
	@Column(name = "JAM",nullable=false)
	private String nama;*/
	@Size(max = 6)
	@Column(name = "DARI",nullable=false)
	private String dariJam;
	@Size(max = 6)
	@Column(name = "SAMPAI",nullable=false)
	private String sampaiJam;
	
	
//	@JoinTable(name = "ID_GURU")
	/*@JoinTable(
		    name="JADWAL_PELAJARAN2",
		    joinColumns=@JoinColumn(name="ID_JADWAL", nullable=false),//,
		    inverseJoinColumns=@JoinColumn(name="ID_GURU", nullable=false))*/
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_GURU")
	@Size(max = 5)
	private Guru guru;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PELAJARAN")
	@Size(max = 5)
	private MataPelajaran pelajaran;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Size(max = 10)
	@JoinColumn(name = "ID_TAHUN_AJARAN")
	private TahunAjaran tahunAjaran;
	
	@Size(max = 5)
	@ManyToOne
	@JoinColumn(name = "ID_KELAS")
	private Kelas kelas;
	
}
