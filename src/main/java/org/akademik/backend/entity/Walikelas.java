package org.akademik.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/*@Data
@Entity
@Table(name = "WALI_KELAS")*/
@Setter
@Getter
public class Walikelas {
//	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "ID_WALIKELAS")
	private String idWalikelas;

//	@OneToOne()
//	@JoinColumn(name = "ID_GURU")
	private Guru guru;
	
//	@OneToOne
//	@JoinColumn(name = "ID_TAHUN_AJARAN")
	private TahunAjaran tahunAjaran;
	
//	@OneToOne
//	@JoinColumn(name = "ID_KELAS")
	private Kelas kelas;
	
}
